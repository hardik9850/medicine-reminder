package com.medicine.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hardik on 11/12/17.
 */

public class MedicineSqlHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "medicine_reminder.db";
    public static final String TABLE_MEDICINE = "medicine";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "medicine_name";
    public static final String COLUMN_DOSAGE = "medicine_dosage";
    public static final String COLUMN_MEDICINE_STATUS = "medicine_is_taken";
    public static final String COLUMN_DAY = "day";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_HOUR = "hour";
    public static final String COLUMN_MINUTE = "minute";

    public static class MedicineStatus {
        public final static int MEDICINE_SCHEDULED = 0;
        public final static int MEDICINE_TAKEN = 1;
        public final static int MEDICINE_SKIPPED = 2;
    }

    private static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_MEDICINE;


    public MedicineSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_MEDICINE_CREATE_TABLE = "CREATE TABLE " + TABLE_MEDICINE + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME + " TEXT UNIQUE," +
                COLUMN_DOSAGE + " INTEGER ," +
                COLUMN_MEDICINE_STATUS + " INTEGER ," +
                COLUMN_DAY + " INTEGER ," +
                COLUMN_MONTH + " INTEGER ," +
                COLUMN_YEAR + " INTEGER ," +
                COLUMN_HOUR + " INTEGER ," +
                COLUMN_MINUTE + " INTEGER )";

        db.execSQL(SQL_MEDICINE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE);
        onCreate(db);
    }


}
