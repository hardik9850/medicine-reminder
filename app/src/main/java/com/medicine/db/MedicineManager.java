package com.medicine.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.medicine.model.Medicine;

import java.util.ArrayList;

/**
 * Created by hardik on 12/12/17.
 */

public class MedicineManager {
    private SQLiteDatabase database;
    private MedicineSqlHelper dbHelper;


    private String[] tableColumns = {
            MedicineSqlHelper.COLUMN_ID,
            MedicineSqlHelper.COLUMN_NAME,
            MedicineSqlHelper.COLUMN_DOSAGE,
            MedicineSqlHelper.COLUMN_MEDICINE_STATUS,
            MedicineSqlHelper.COLUMN_DAY,
            MedicineSqlHelper.COLUMN_MONTH,
            MedicineSqlHelper.COLUMN_YEAR,
            MedicineSqlHelper.COLUMN_HOUR,
            MedicineSqlHelper.COLUMN_MINUTE};


    public MedicineManager(Context context) {
        dbHelper = new MedicineSqlHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Medicine createMedicine(String name, int dose, int day, int month, int year, int hour, int mimute) {
        ContentValues values = new ContentValues();
        values.put(MedicineSqlHelper.COLUMN_NAME, name);
        values.put(MedicineSqlHelper.COLUMN_DOSAGE, dose);
        values.put(MedicineSqlHelper.COLUMN_DAY, day);
        values.put(MedicineSqlHelper.COLUMN_MONTH, month);
        values.put(MedicineSqlHelper.COLUMN_YEAR, year);
        values.put(MedicineSqlHelper.COLUMN_HOUR, hour);
        values.put(MedicineSqlHelper.COLUMN_MINUTE, mimute);

        long insertId = database.insert(MedicineSqlHelper.TABLE_MEDICINE, null,
                values);

        Cursor cursor = database.query(MedicineSqlHelper.TABLE_MEDICINE,
                tableColumns, MedicineSqlHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Medicine medicine = cursorToMedicine(cursor);
        cursor.close();

        return medicine;
    }

    private Medicine cursorToMedicine(Cursor cursor) {
        Medicine medicine = new Medicine();
        if (cursor == null)
            return medicine;
        medicine.setId(cursor.getInt(0));
        medicine.setName(cursor.getString(1));
        medicine.setDosage(cursor.getInt(2));
        medicine.setStatus(cursor.getInt(3));
        medicine.setDay(cursor.getInt(4));
        medicine.setMonth(cursor.getInt(5));
        medicine.setYear(cursor.getInt(6));
        medicine.setHour(cursor.getInt(7));
        medicine.setMinute(cursor.getInt(8));
        return medicine;
    }

    public void updateMedicineStatus(Medicine medicine) {
        int id = medicine.getId();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MedicineSqlHelper.COLUMN_MEDICINE_STATUS, medicine.getStatus());

        database.update(MedicineSqlHelper.TABLE_MEDICINE,
                contentValues, MedicineSqlHelper.COLUMN_ID + "=" + id, null);
    }

    public ArrayList<Medicine> getAllMedicines() {
        ArrayList<Medicine> medicineArrayList = new ArrayList<>();

        Cursor cursor = database.query(MedicineSqlHelper.TABLE_MEDICINE,
                tableColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Medicine medicine = cursorToMedicine(cursor);
            medicineArrayList.add(medicine);
            cursor.moveToNext();
        }
        cursor.close();
        return medicineArrayList;
    }

    public ArrayList<Medicine> getAllMedicinesByMonth(int monthIndex, int status) {
        ArrayList<Medicine> medicineArrayList = new ArrayList<Medicine>();
        String whereClause = MedicineSqlHelper.COLUMN_MONTH + " = ? AND " + MedicineSqlHelper.COLUMN_MEDICINE_STATUS + " = ? ";
        String[] whereArgs = {String.valueOf(monthIndex), String.valueOf(status)};

        Cursor cursor = database.query(MedicineSqlHelper.TABLE_MEDICINE,
                tableColumns, whereClause,
                whereArgs, null, null, null);
        while (cursor.moveToNext()) {
            Medicine medicine = cursorToMedicine(cursor);
            medicineArrayList.add(medicine);
        }
        cursor.close();
        return medicineArrayList;
    }

    public Medicine getMedicineById(int medicineId) {
        String whereClause = MedicineSqlHelper.COLUMN_ID + " = ? ";
        String[] whereArgs = {String.valueOf(medicineId)};

        Cursor cursor = database.query(MedicineSqlHelper.TABLE_MEDICINE,
                tableColumns, whereClause,
                whereArgs, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            Medicine medicine = cursorToMedicine(cursor);
            return medicine;
        }
        return null;
    }
}
