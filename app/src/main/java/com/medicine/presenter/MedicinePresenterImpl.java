package com.medicine.presenter;

import com.medicine.db.MedicineManager;
import com.medicine.db.MedicineSqlHelper;
import com.medicine.model.Medicine;
import com.medicine.ui.MedicineView;

import java.util.ArrayList;

/**
 * Created by hardik on 11/12/17.
 */

public class MedicinePresenterImpl implements MedicinePresenter {
    private MedicineView medicineView;
    private MedicineManager medicineManager;

    public MedicinePresenterImpl(MedicineView medicineView) {
        this.medicineView = medicineView;
        medicineManager = new MedicineManager(medicineView.getContext());
    }

    @Override
    public void onNewEntryClicked() {
        medicineView.onNewEntryCreated();
    }

    @Override
    public void onActivityCreated() {
        medicineManager.open();
        ArrayList<Medicine> medicineList = medicineManager.getAllMedicines();
        if (medicineList == null) {
            return;
        }
        if (medicineList.size() > 0) {
            medicineView.shouldShowMedicineList(medicineList);
        }
    }

    @Override
    public void onMedicineClicked(int position, Medicine medicine) {
        medicineView.onChangeMedicineStatusClicked(position, medicine);
    }

    @Override
    public void changeMedicineStatus(int position, Medicine medicine) {
        medicineManager.updateMedicineStatus(medicine);
        medicineView.notifyItemChanged(position);
    }

    @Override
    public void reloadData() {
        ArrayList<Medicine> medicineList = medicineManager.getAllMedicines();
        if (medicineList.size() > 0) {
            medicineView.shouldShowMedicineList(medicineList);
        }
    }

    @Override
    public void onDestroy() {
        medicineManager.close();
    }

    @Override
    public void onFilterClicked(int filterType) {
        switch (filterType) {
            case NO_FILTER: {
                ArrayList<Medicine> medicineList = medicineManager.getAllMedicines();
                if (medicineList.size() > 0) {
                    medicineView.shouldShowMedicineList(medicineList);
                }
            }
            break;
            case FILTER_BY_SKIPPED: {
                medicineView.showSkippedMedicines();
            }
            break;
            case FILTER_BY_TAKEN: {
                medicineView.showCompletedMedicines();
            }
            break;
        }
    }

    @Override
    public void loadDataByMonth(int month, int which) {
        int status = 0;
        if (which == FILTER_BY_TAKEN) {
            status = MedicineSqlHelper.MedicineStatus.MEDICINE_TAKEN;
        } else if (which == FILTER_BY_SKIPPED) {
            status = MedicineSqlHelper.MedicineStatus.MEDICINE_SKIPPED;
        }
        ArrayList<Medicine> list = medicineManager.getAllMedicinesByMonth(month, status);
        if (list == null)
            return;
        medicineView.shouldShowMedicineList(list);
    }
}
