package com.medicine.presenter;

/**
 * Created by hardik on 11/12/17.
 */

public interface AddMedicinePresenter {
    void onAlarmSetClicked(String medicineName, String medicineDosage, String day, String time);

}