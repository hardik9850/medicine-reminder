package com.medicine.presenter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.medicine.alarm.AlarmReceiver;
import com.medicine.db.MedicineManager;
import com.medicine.model.Medicine;
import com.medicine.ui.AddMedicineView;

import java.util.Calendar;

import static com.medicine.alarm.AlarmReceiver.TAG_MEDICINE_ID;

/**
 * Created by hardik on 11/12/17.
 */

public class AddMedicinePresenterImpl implements AddMedicinePresenter {
    private final MedicineManager medicineManager;
    private AddMedicineView addMedicineView;

    public AddMedicinePresenterImpl(AddMedicineView addMedicineView) {
        this.addMedicineView = addMedicineView;
        medicineManager = new MedicineManager(addMedicineView.getContext());
    }

    @Override
    public void onAlarmSetClicked(String medicineName, String medicineDosage, String date, String time) {
        medicineManager.open();
        if (TextUtils.isEmpty(medicineName)) {
            addMedicineView.emptyMedicineName();
            return;
        }
        if (TextUtils.isEmpty(medicineDosage)) {
            addMedicineView.emptyMedicineDosage();
            return;
        }
        if (TextUtils.isEmpty(date)) {
            addMedicineView.emptyMedicineDay();
            return;
        }
        if (TextUtils.isEmpty(time)) {
            addMedicineView.emptyMedicineTime();
            return;
        }
        int dosage = Integer.parseInt(medicineDosage);
        String[] dateArray = date.split("/");
        String[] timeArray = time.split(":");
        int day = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int year = Integer.parseInt(dateArray[2]);

        int hour = Integer.parseInt(timeArray[0]);
        int minutes = Integer.parseInt(timeArray[1]);
        Medicine medicine = medicineManager.createMedicine(medicineName, dosage, day, month, year, hour, minutes);
        if (medicine != null) {
            Context context = addMedicineView.getContext();
            scheduleAlarm(context, medicine);
            medicineManager.close();
            addMedicineView.onAlarmSuccessfullyCreated();
        }
    }

    private void scheduleAlarm(Context context, Medicine medicine) {
        if (medicine == null)
            return;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, medicine.getYear());
        calendar.set(Calendar.MONTH, medicine.getMonth()-1);
        calendar.set(Calendar.DAY_OF_MONTH, medicine.getDay());
        calendar.set(Calendar.HOUR_OF_DAY, medicine.getHour());
        calendar.set(Calendar.MINUTE, medicine.getMinute());
        calendar.set(Calendar.SECOND, 0);

        Intent intentAlarm = new Intent(context, AlarmReceiver.class);
        int id = medicine.getId();
        intentAlarm.putExtra(TAG_MEDICINE_ID, id);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, id, intentAlarm, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }
}
