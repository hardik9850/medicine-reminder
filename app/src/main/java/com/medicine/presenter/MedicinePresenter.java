package com.medicine.presenter;

import com.medicine.model.Medicine;

/**
 * Created by hardik on 11/12/17.
 */

public interface MedicinePresenter {

    int NO_FILTER = 0;
    int FILTER_BY_TAKEN = 1;
    int FILTER_BY_SKIPPED = 2;

    void onNewEntryClicked();

    void onActivityCreated();

    void onMedicineClicked(int position, Medicine medicine);

    void changeMedicineStatus(int position, Medicine medicine);

    void reloadData();

    void onDestroy();

    void onFilterClicked(int filterType);

    void loadDataByMonth(int filterOption, int which);
}
