package com.medicine.callback;

import com.medicine.model.Medicine;

/**
 * Created by hardik on 12/12/17.
 */

public interface OnItemClickListener {
    void onItemClicked(int position);
}
