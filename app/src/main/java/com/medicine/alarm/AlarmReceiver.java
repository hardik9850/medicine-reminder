package com.medicine.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.medicine.ui.AlarmActivity;

/**
 * Created by hardik on 12/12/17.
 */

public class AlarmReceiver extends BroadcastReceiver {
    public static final String TAG_MEDICINE_ID = "medicineId";
    @Override
    public void onReceive(Context context, Intent intent) {
        int medicineId = intent.getExtras().getInt(TAG_MEDICINE_ID);
        Intent scheduledIntent = new Intent(context, AlarmActivity.class);
        scheduledIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        scheduledIntent.putExtra(TAG_MEDICINE_ID, medicineId);
        context.startActivity(scheduledIntent);
    }
}
