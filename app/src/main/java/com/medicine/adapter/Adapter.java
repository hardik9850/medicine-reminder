package com.medicine.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medicine.callback.OnItemClickListener;
import com.medicine.callback.OnMedicineClicked;
import com.medicine.R;
import com.medicine.db.MedicineSqlHelper;
import com.medicine.model.Medicine;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by hardik on 12/12/17.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MedicineViewHolder> implements OnItemClickListener {

    private ArrayList<Medicine> medicines;
    private OnMedicineClicked onMedicineClicked;
    private NumberFormat formatter = new DecimalFormat("00");
    private Context context;

    public Adapter(OnMedicineClicked onMedicineClicked) {
        medicines = new ArrayList<>();
        this.onMedicineClicked = onMedicineClicked;
    }

    public void setData(ArrayList<Medicine> data) {
        medicines = data;
        notifyDataSetChanged();
    }

    @Override
    public MedicineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.adapter_medicine_detail, parent, false);
        context = v.getContext();
        return new MedicineViewHolder(v, this);
    }


    @Override
    public void onBindViewHolder(MedicineViewHolder holder, int position) {
        Medicine medicine = medicines.get(position);
        if (medicine == null)
            return;
        String name = medicine.getName();
        String date = medicine.getDay() + "/" + medicine.getMonth() + "/" + medicine.getYear();
        String time = formatter.format(medicine.getHour()) + ":" + formatter.format(medicine.getMinute());

        String dateAndTime = date + " At " + time;
        String dosage = String.valueOf(medicine.getDosage());
        int statusVal = medicine.getStatus();
        String status = "";
        int color = ContextCompat.getColor(context, R.color.colorGreen);
        switch (statusVal) {
            case MedicineSqlHelper.MedicineStatus.MEDICINE_SCHEDULED: {
                color = ContextCompat.getColor(context, R.color.colorPrimary);
                status = "Scheduled";
            }
            break;
            case MedicineSqlHelper.MedicineStatus.MEDICINE_SKIPPED: {
                color = ContextCompat.getColor(context, android.R.color.holo_red_light);
                status = "Skipped";
            }
            break;
            case MedicineSqlHelper.MedicineStatus.MEDICINE_TAKEN: {
                color = ContextCompat.getColor(context, R.color.colorGreen);
                status = "Taken";
            }
            break;
        }
        holder.name.setText(name);
        holder.dosage.setText(dosage);
        holder.time.setText(dateAndTime);
        holder.status.setText(status);
        holder.status.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return medicines.size();
    }


    @Override
    public void onItemClicked(int position) {
        Medicine medicine = medicines.get(position);
        if (onMedicineClicked == null)
            return;
        if (medicine == null)
            return;
        onMedicineClicked.onMedicineClicked(position, medicine);
    }

    static class MedicineViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;
        TextView dosage;
        TextView time;
        TextView status;
        OnItemClickListener onItemClickListener;

        public MedicineViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.onItemClickListener = onItemClickListener;
            name = (TextView) itemView.findViewById(R.id.txt_medicine_name);
            dosage = (TextView) itemView.findViewById(R.id.txt_dosage);
            time = (TextView) itemView.findViewById(R.id.txt_date);
            status = (TextView) itemView.findViewById(R.id.txt_status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position == -1)
                return;
            onItemClickListener.onItemClicked(position);
        }
    }
}
