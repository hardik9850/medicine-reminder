package com.medicine.ui;

import android.content.Context;

import com.medicine.model.Medicine;

import java.util.ArrayList;

/**
 * Created by hardik on 11/12/17.
 */

public interface MedicineView {
    void onNewEntryCreated();
    void shouldShowMedicineList(ArrayList<Medicine> list);
    Context getContext();
    void onChangeMedicineStatusClicked(int position,Medicine medicine);
    void notifyItemChanged(int position);
    void showCompletedMedicines();
    void showSkippedMedicines();
}
