package com.medicine.ui;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.medicine.R;
import com.medicine.presenter.AddMedicinePresenter;
import com.medicine.presenter.AddMedicinePresenterImpl;

import java.util.Calendar;

/**
 * Created by hardik on 11/12/17.
 */

public class AddMedicineActivity extends FragmentActivity implements View.OnClickListener, AddMedicineView {

    EditText edtMedicineName;
    EditText edtMedicineDosage;
    EditText edtMedicineDay;
    EditText edtMedicineTime;

    private Context context;
    private AddMedicinePresenter addMedicinePresenter;
    private int day, month, year;
    private int hour, minute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_medicine);
        context = this;
        edtMedicineName = (EditText) findViewById(R.id.edt_medicine_name);
        edtMedicineDosage = (EditText) findViewById(R.id.edt_dosage_count);
        edtMedicineDay = (EditText) findViewById(R.id.edt_day);
        edtMedicineTime = (EditText) findViewById(R.id.edt_time);
        findViewById(R.id.btn_submit).setOnClickListener(this);
        addMedicinePresenter = new AddMedicinePresenterImpl(this);
        SetTime setTime = new SetTime(edtMedicineTime);
        SetDate setDate = new SetDate(edtMedicineDay);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit: {
                String medicineName = edtMedicineName.getText().toString();
                String medicineDosage = edtMedicineDosage.getText().toString();
                String medicineDay = edtMedicineDay.getText().toString();
                String medicineTime = edtMedicineTime.getText().toString();
                addMedicinePresenter.onAlarmSetClicked(medicineName, medicineDosage, medicineDay, medicineTime);
            }
            break;
        }
    }

    @Override
    public void emptyMedicineName() {
        Toast.makeText(context, R.string.add_medicine,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void emptyMedicineDosage() {
        Toast.makeText(context, R.string.add_dosage,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void emptyMedicineTime() {
        Toast.makeText(context, R.string.add_time,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void emptyMedicineDay() {
        Toast.makeText(context, R.string.add_date,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAlarmSuccessfullyCreated() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    class SetTime implements View.OnFocusChangeListener, TimePickerDialog.OnTimeSetListener {

        private EditText editText;
        private Calendar myCalendar;

        public SetTime(EditText editText) {
            this.editText = editText;
            this.editText.setOnFocusChangeListener(this);
            this.myCalendar = Calendar.getInstance();

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                minute = myCalendar.get(Calendar.MINUTE);
                new TimePickerDialog(context, this, hour, minute, true).show();
            }
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            this.editText.setText(new StringBuilder().append(hourOfDay).append(":").append(minute).toString());
        }
    }

    class SetDate implements View.OnFocusChangeListener, OnDateSetListener {

        private EditText editText;
        private Calendar myCalendar;

        public SetDate(EditText editText) {
            this.editText = editText;
            this.editText.setOnFocusChangeListener(this);
            this.myCalendar = Calendar.getInstance();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                int month = myCalendar.get(Calendar.MONTH);
                int year = myCalendar.get(Calendar.YEAR);
                int day = myCalendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(context, this, year, month, day).show();
            }
        }


        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            month++;//Incrementing month by 1, by default month starts from 0
            this.editText.setText(
                    new StringBuilder().append(dayOfMonth).append("/").append(month).append("/").append(year)
                            .toString());
        }
    }


}
