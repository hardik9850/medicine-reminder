package com.medicine.ui;

import android.app.Activity;

/**
 * Created by hardik on 12/12/17.
 */

import android.media.Ringtone;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.medicine.R;
import com.medicine.db.MedicineManager;
import com.medicine.model.Medicine;

import static android.media.RingtoneManager.TYPE_RINGTONE;
import static android.media.RingtoneManager.getDefaultUri;
import static android.media.RingtoneManager.getRingtone;

public class AlarmActivity extends Activity {
    private MedicineManager medicineManager;
    private Uri notification;
    private Ringtone alarmRingtone;
    private int medicineId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        notification = getDefaultUri(TYPE_RINGTONE);
        alarmRingtone = getRingtone(getApplicationContext(), notification);
        alarmRingtone.play();

        medicineId = getIntent().getExtras().getInt("medicineId");
        medicineManager = new MedicineManager(this);
        medicineManager.open();
        Medicine medicine = medicineManager.getMedicineById(medicineId);
        if (medicine == null) {
            return;
        }
        medicineManager.close();
        ((TextView) findViewById(R.id.txt_medicine_name)).setText("Time to take ".concat(medicine.getName()));
        findViewById(R.id.btn_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmRingtone.stop();
                finish();
            }
        });
    }

}