package com.medicine.ui;

import android.content.Context;

/**
 * Created by hardik on 11/12/17.
 */

public interface AddMedicineView {
    void emptyMedicineName();
    void emptyMedicineDosage();
    void emptyMedicineTime();
    void emptyMedicineDay();
    void onAlarmSuccessfullyCreated();

    Context getContext();
}
