package com.medicine.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.medicine.R;
import com.medicine.adapter.Adapter;
import com.medicine.callback.OnMedicineClicked;
import com.medicine.db.MedicineSqlHelper;
import com.medicine.model.Medicine;
import com.medicine.presenter.MedicinePresenter;
import com.medicine.presenter.MedicinePresenterImpl;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MedicineView, OnMedicineClicked {

    private FloatingActionButton fabAddMedicine;
    private MedicinePresenter medicinePresenter;
    private final int REQUEST_ADD_MEDICINE = 100;
    private Adapter medicineAdapter;
    private RecyclerView recyclerView;
    private TextView txtPlaceHolder;

    private final int NO_FILTER = 0;
    private final int FILTER_BY_TAKEN = 1;
    private final int FILTER_BY_SKIPPED = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        medicinePresenter = new MedicinePresenterImpl(this);
        bindData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_taken: {
                medicinePresenter.onFilterClicked(FILTER_BY_TAKEN);
            }
            break;
            case R.id.menu_item_skipped: {
                medicinePresenter.onFilterClicked(FILTER_BY_SKIPPED);
            }
            break;
            case R.id.menu_item_all:{
                medicinePresenter.onFilterClicked(NO_FILTER);
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void bindData() {
        txtPlaceHolder = (TextView) findViewById(R.id.txt_place_holder);
        fabAddMedicine = (FloatingActionButton) findViewById(R.id.fab_add_product);
        fabAddMedicine.setOnClickListener(this);

        medicineAdapter = new Adapter(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_medicines);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                this, linearLayoutManager.getOrientation());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(medicineAdapter);
        recyclerView.addItemDecoration(mDividerItemDecoration);
        medicinePresenter.onActivityCreated();
    }

    @Override
    public void onClick(View v) {
        medicinePresenter.onNewEntryClicked();
    }


    @Override
    public void onNewEntryCreated() {
        Intent intent = new Intent(this, AddMedicineActivity.class);
        startActivityForResult(intent, REQUEST_ADD_MEDICINE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            medicinePresenter.reloadData();
        }
    }

    @Override
    public void shouldShowMedicineList(ArrayList<Medicine> medicineList) {
        txtPlaceHolder.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        medicineAdapter.setData(medicineList);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        medicinePresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onChangeMedicineStatusClicked(final int position, final Medicine medicine) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Update medicine status");
        String[] types = {"Taken", "Skipped"};
        b.setItems(types, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        medicine.setStatus(MedicineSqlHelper.MedicineStatus.MEDICINE_TAKEN);
                        medicinePresenter.changeMedicineStatus(position, medicine);
                        break;
                    case 1:
                        medicine.setStatus(MedicineSqlHelper.MedicineStatus.MEDICINE_SKIPPED);
                        medicinePresenter.changeMedicineStatus(position, medicine);
                        break;
                }
            }
        });
        b.show();
    }

    @Override
    public void notifyItemChanged(int position) {
        medicineAdapter.notifyItemChanged(position);
    }

    @Override
    public void showCompletedMedicines() {
        showMonthList(FILTER_BY_TAKEN);
    }

    @Override
    public void showSkippedMedicines() {
        showMonthList(FILTER_BY_SKIPPED);
    }

    private void showMonthList(final int filterOption) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Select month");
        String[] types = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        b.setItems(types, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (which < 0 && which > 12)
                    return;
                which += 1;
                medicinePresenter.loadDataByMonth(which, filterOption);
            }
        });
        b.show();
    }


    @Override
    public void onMedicineClicked(int position, Medicine medicine) {
        medicinePresenter.onMedicineClicked(position, medicine);
    }
}
